/**
 * Created by José Guerrero on 28/03/2017.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Users = mongoose.model('Users'),
    Counters = mongoose.model('Counters');

// Función para asignar valor para id autoincremental 1 a 1
function getNextSequences() {
   /* var ret = '';
        Counters.findByIdAndUpdate({_id: 'userId'}, {$inc: {sequence:1}}).exec(function(error, counter) {
        if(error)
            return next(error);
        ret = counter.sequence;
        //next();
    });

    console.log('return: ' + ret);
    return ret.sequence;*/

    var userId = '';

    Counters.findById('userId').exec(function (err, count) {
        if (err) return next(err);
        count.sequence += 1;
        userId = count.sequence;

        count.save(function (err, counter) {
            if (err) return next(err);
            console.log('ID: ' + counter.sequence);
            console.log('return: ' + userId);
            return userId;
        });
    });



}

// URL Base
module.exports = function (app) {
    app.use('/api/v1', router);
};

// GET - All Users
router.get('/users', function (request, response, next) {
    Users.find(function (err, users) {
        if (err) return next(err);
        response.json(users);
    });
});

// POST - Create User
router.post('/user', function (request, response, next) {
    var user = new Users();

    Counters.findByIdAndUpdate({_id: 'userId'}, {$inc: {sequence: 1}}).exec(function(error, counter) {
        if(error) return next(error);
        user.id = counter.sequence;
        user.avatar = request.body.avatar;
        user.username = request.body.username;
        user.firstName = request.body.firstName;
        user.lastName = request.body.lastName;
        user.phone = request.body.phone;
        user.gender = request.body.gender;
        user.birthday = request.body.birthday;

        if(user.id !== null){
            user.save(function (err, user) {
                if (err) return next(err);
                response.json(user);
            });
        }
        else{
            console.log('user.id es: '+ user.id);
        }
    });

   /* Counters.findById('userId').exec(function (err, count) {
        if (err) return next(err);
        count.sequence += 1;

        count.save(function (err, counter) {
            if (err) return next(err);
            user.id = counter.sequence;
            user.avatar = request.body.avatar;
            user.username = request.body.username;
            user.firstName = request.body.firstName;
            user.lastName = request.body.lastName;
            user.phone = request.body.phone;
            user.gender = request.body.gender;
            user.birthday = request.body.birthday;

            if(user.id !== null){
                user.save(function (err, user) {
                    if (err) return next(err);
                    response.json(user);
                });
            }
            else{
                console.log('user.id es: '+ user.id);
            }
        });
    });
*/

});

// PUT - Update User
router.put('/user/:id', function (request, response) {
    Users.findById(request.params.id).exec(function (err, user) {

        console.log(request.body);
        user.avatar = request.body.avatar;
        user.username = request.body.username;
        user.firstName = request.body.firstName;
        user.lastName = request.body.lastName;
        user.phone = request.body.phone;
        user.gender = request.body.gender;
        user.birthday = request.body.birthday;

        user.save(function (err) {
            if (err) response.send(err);
            response.json(user);
        });
    });
});

// DELETE - Delete User
router.delete('/user/:id', function (request, response) {
    Users.findByIdAndRemove(request.params.id).exec(function (err) {
        console.log(request.params.id);
        if (err) response.send(err);
        response.json({message: 'The user has been deleted.'})
    });
});

// GET - One User
router.get('/user/:id', function (request, response, next) {
    Users.findById(request.params.id).exec(function (err, user) {
        if (err) return next(err);

        user.sequence += 1;
        console.log(user.sequence);

        user.save(function (err, user) {
            if (err) return next(err);
            response.json(user);
        });
    });
});

// GET - Search User
router.get('/user/search/:conditions/:value', function (request, response, next) {
    var condition = request.params.conditions;
    var value = request.params.value;
    var where = {};

    // Validar si el value es numérico
    if (!isNaN(value)){
        where[condition] = {$in:[Number(value)], $exists: true, $not: {$size: 0}}
    }
    else{
        where[condition] = value;
    }

    Users.find().where(where).exec(function (err, user) {
        if (err) return next(err);
        console.log(err);
        response.json(user);
    });
});