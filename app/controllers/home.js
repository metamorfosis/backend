var express = require('express'),
  router = express.Router();

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res) {
    res.render('index', {
        title: 'SocialH4ck'
    });
});

router.get('/wiki/users', function (request, response) {
    response.render('wiki', {
        title: 'Users'
    });
});
