/**
 * Created by José Guerrero on 07/04/2017.
 */

// Counters model

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CountersSchema = new Schema({
    _id: {type: String, required: true},
    sequence: {type: Number}
});

mongoose.model('Counters', CountersSchema);