/**
 * Created by José Guerrero on 28/03/2017.
 */

// Users model

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UsersSchema = new Schema({
    id: {type: Number},
    avatar: {type: String},
    username: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    phone: {type: String, required: true},
    gender: {type: String, enum: ['Male', 'Female']},
    birthday: {type: Date, required: true}
});

UsersSchema.virtual('date')
    .get(function(){
        return this._id.getTimestamp();
    });

mongoose.model('Users', UsersSchema);

