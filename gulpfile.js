var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload');

var paths = {
    css: 'public/css/',
    public: 'public/',
    js: 'public/js/',
    font: 'public/fonts/',
    img: 'public/img/',
    node: 'node_modules/',
    fonts: 'node_modules/**/*.{ttf,woff,woff2,eof,svg}'
};

gulp.task('develop', function () {
  livereload.listen();
  nodemon({
    script: 'app.js',
    ext: 'js coffee ejs css',
    stdout: false
  }).on('readable', function () {
    this.stdout.on('data', function (chunk) {
      if(/^Express server listening on port/.test(chunk)){
        livereload.changed(__dirname);
      }
    });
    this.stdout.pipe(process.stdout);
    this.stderr.pipe(process.stderr);
  });
});

// Tareas para mover archivos al directorio Public
gulp.task('moveCss', function () {
    gulp.src([
        paths.node + 'bootstrap/dist/css/bootstrap.min.css',
        paths.node + 'bootstrap/dist/css/bootstrap.min.css.map',
        paths.node + 'font-awesome/css/font-awesome.css'
    ])
        .pipe(gulp.dest(paths.css))
        .pipe(notify('Finalizó la tarea Mover CSS'));
});

gulp.task('moveJs', function () {
    gulp.src([
        paths.node + 'jquery/dist/jquery.min.js',
        paths.node + 'bootstrap/dist/js/bootstrap.min.js'
    ])
        .pipe(gulp.dest(paths.js))
        .pipe(notify('Finalizó la tarea Mover JS'));
});

gulp.task('copyFonts', function() {
    return gulp.src(paths.fonts)
        .pipe(rename({
            dirname: 'fonts'
        }))
        .pipe(gulp.dest(paths.public))
        .pipe(notify('Finalizó la tarea copyFonts'));
});

gulp.task('default', [
    'moveJs',
    'moveCss',
    'copyFonts',
    'develop'
]);
