var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'apinode'
        },
        port: process.env.PORT || 3500,
        db: 'mongodb://localhost:27017/apinode-dev'
    },

    test: {
        root: rootPath,
        app: {
            name: 'apinode'
        },
        port: process.env.PORT || 3500,
        db: 'mongodb://localhost:27017/apinode-test'
    },

    production: {
        root: rootPath,
        app: {
            name: 'apinode'
        },
        port: process.env.PORT || 3500,
        db: 'mongodb://localhost:27017/apinode-production'
    }
};

module.exports = config[env];
